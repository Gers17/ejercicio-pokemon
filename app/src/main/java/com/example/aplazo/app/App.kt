package com.example.aplazo.app

import android.app.Application
import io.ktor.util.*
import org.koin.android.java.KoinAndroidApplication.create
import org.koin.core.KoinApplication
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        val koin = create(this).modules(AppModules.getModules(applicationContext))

        startKoin(koin)
    }


}