package com.example.aplazo.app

import android.content.Context
import com.example.aplazo.data.network.RetrofitBuilder
import com.example.aplazo.data.repository.PokemonRepository
import com.example.aplazo.domain.usecase.*
import com.example.aplazo.domain.usecase.impl.GetCatalogs
import com.example.aplazo.domain.usecase.impl.GetPokemonDetail
import com.example.aplazo.ui.catalogs.CatalogsViewModel
import com.example.aplazo.ui.detailpokemon.PokemonDetailViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

class AppModules {

    companion object {

        fun getModules(context: Context): Module {
            return module {


                single { RetrofitBuilder.buildApiService() }

                //Catalogs
                single { PokemonRepository(get()) }
                single<IGetCatalogs> { GetCatalogs(get()) }
                single<IGetPokemonDetail> { GetPokemonDetail(get()) }

                // MyViewModel ViewModel
                viewModel { CatalogsViewModel(get()) }
                viewModel { PokemonDetailViewModel(get()) }


            }
        }




    }

}

