package com.example.aplazo.data

import android.util.Log
import com.example.aplazo.domain.BaseResultV2
import com.example.aplazo.domain.ErrorResult
import retrofit2.Response

abstract class DataSource {

    private val logTAG = DataSource::class.java.simpleName

    protected suspend fun <I, O> getResult(
        call: suspend () -> Response<I>,
        mapper: (I?) -> O
    ): BaseResultV2<O, String> {
        try {
            val response = call()
            if (response.isSuccessful) {
                return BaseResultV2.Success(mapper(response.body()))
            }
            return (BaseResultV2.Error(" ${response.code()} ${response.headers()}"))
        } catch (e: Exception) {
            Log.e(logTAG, e.message ?: e.toString())
            return (BaseResultV2.Error(e.message ?: e.toString()))
        }
    }


    protected suspend fun <I, O> getResultWithCode(
        call: suspend () -> Response<I>,
        mapper: (Pair<Int, I?>) -> O
    ): BaseResultV2<O, ErrorResult> {
        try {
            val response = call()
            if (response.isSuccessful) {
                return BaseResultV2.Success(mapper(response.code() to response.body()))
            }
            return (BaseResultV2.Error(
                ErrorResult(
                    response.code(),
                    response.message()
                )
            ))
        } catch (e: Exception) {
            Log.e(logTAG, e.message ?: e.toString())
            return (BaseResultV2.Error(ErrorResult(0, e.message ?: e.toString())))
        }
    }

}