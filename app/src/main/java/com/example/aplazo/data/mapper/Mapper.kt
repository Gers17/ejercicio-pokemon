package com.example.aplazo.data.mapper

interface Mapper<I, O> {

    fun transform(input: I?): O

}