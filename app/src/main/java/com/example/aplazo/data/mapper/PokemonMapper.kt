package com.example.aplazo.data.mapper

import com.example.aplazo.data.network.response.DataPokemon
import com.example.aplazo.data.network.response.DataPokemons
import com.example.aplazo.domain.model.Pokemon
import com.example.aplazo.domain.model.Pokemons

class PokemonMapper : Mapper<DataPokemons,Pokemons> {


    override fun transform(input: DataPokemons?): Pokemons {

        return if (input == null) {
            Pokemons()
        } else {
            Pokemons(
                count = input.count ?: 0,
                next = input.next ?: "",
                previous = input.previous ?: Any(),
                dataPokemons = transform(input.dataPokemons)
            )
        }
    }

    fun transform(input: List<DataPokemon>?) : List<Pokemon>{
        return input?.map {
            Pokemon(
                it.name ?: "",
                it.url ?: "",
            )
        } ?: emptyList()
    }

}