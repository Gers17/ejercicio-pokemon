package sengiapps.loadsapp.data.network

import com.example.aplazo.data.network.response.DataDetailPokemon
import com.example.aplazo.data.network.response.DataPokemons
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {


    @GET("pokemon")
    suspend fun getCatalogs(
        @Query("limit") limit: String ,
        @Query("offset") offset: String = "0"
    ) : Response<DataPokemons>

    @GET("pokemon/{id_pokemon}")
    suspend fun getDetail(
        @Path("id_pokemon") idPokemon: String,
    ) : Response<DataDetailPokemon>

}