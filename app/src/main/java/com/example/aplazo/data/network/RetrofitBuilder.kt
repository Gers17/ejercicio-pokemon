package com.example.aplazo.data.network


import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import sengiapps.loadsapp.data.network.ApiService
import java.util.concurrent.TimeUnit

object RetrofitBuilder {


    private val baseUrl = "https://pokeapi.co/api/v2/"

    private val okHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(40, TimeUnit.SECONDS)
            .callTimeout(30, TimeUnit.SECONDS)
            .build()

    private fun getClient() =
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .build()

    fun buildApiService(): ApiService = getClient().create(ApiService::class.java)

}