package com.example.aplazo.data.network.response

data class AbilityX(
    val name: String?,
    val url: String?
)