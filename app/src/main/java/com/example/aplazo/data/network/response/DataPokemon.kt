package com.example.aplazo.data.network.response

data class DataPokemon(
    val name: String?,
    val url: String?
)