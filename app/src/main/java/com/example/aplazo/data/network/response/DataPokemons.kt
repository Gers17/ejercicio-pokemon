package com.example.aplazo.data.network.response

import com.google.gson.annotations.SerializedName

data class DataPokemons(
    val count: Int?,
    val next: String?,
    val previous: Any?,
    @SerializedName("results")
    val dataPokemons: List<DataPokemon>?
)