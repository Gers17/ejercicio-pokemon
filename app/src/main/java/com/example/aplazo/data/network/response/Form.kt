package com.example.aplazo.data.network.response

data class Form(
    val name: String?,
    val url: String?
)