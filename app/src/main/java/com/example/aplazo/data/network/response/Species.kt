package com.example.aplazo.data.network.response

data class Species(
    val name: String?,
    val url: String?
)