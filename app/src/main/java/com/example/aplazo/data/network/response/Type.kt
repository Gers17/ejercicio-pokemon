package com.example.aplazo.data.network.response

data class Type(
    val slot: Int?,
    val type: TypeX?
)