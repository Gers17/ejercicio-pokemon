package com.example.aplazo.data.network.response

data class TypeX(
    val name: String?,
    val url: String?
)