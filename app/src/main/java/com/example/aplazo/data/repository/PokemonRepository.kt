package com.example.aplazo.data.repository

import com.example.aplazo.data.DataSource
import com.example.aplazo.data.mapper.PokemonMapper
import sengiapps.loadsapp.data.network.ApiService

class PokemonRepository (private val apiService: ApiService) : DataSource(){

    private val mapper = PokemonMapper()

    suspend fun getPokemons(limit: String) = getResult(
        {apiService.getCatalogs(limit)},
        {mapper.transform(it)}
    )

    suspend fun getDetailPokemon(idPokemon: String) = getResult(
        {apiService.getDetail(idPokemon)},
        {it}
    )

}