package com.example.aplazo.domain

@Deprecated("Use BaseResultV2 for DataResource() compatibility", ReplaceWith("BaseResultV2"))
sealed class BaseResult<out T : Any, out U : Any> {
    data class Success<T : Any>(val data: T) : BaseResult<T, Nothing>()
    data class Error<U : Any>(val error: U) : BaseResult<Nothing, U>()
}

sealed class BaseResultV2<out T, out U> {
    data class Success<out T>(val data: T) : BaseResultV2<T, Nothing>()
    data class Error<out U>(val error: U) : BaseResultV2<Nothing, U>()
}
