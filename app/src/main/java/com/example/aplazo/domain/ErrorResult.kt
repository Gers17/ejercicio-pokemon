package com.example.aplazo.domain

data class ErrorResult(
    val code: Int = 0,
    val message: String = ""
)