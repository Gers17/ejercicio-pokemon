package com.example.aplazo.domain.model

import com.google.gson.annotations.SerializedName

data class Catalog(
    val id: String = "",
    val category: String = "",
    val thumb: String = "",
    val description: String = ""
)
