package com.example.aplazo.domain.model

import java.text.FieldPosition

data class Pokemon(
    val name: String = "",
    val imagen: String = "",
)
