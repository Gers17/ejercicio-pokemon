package com.example.aplazo.domain.model

import com.example.aplazo.data.network.response.DataPokemon

data class Pokemons(
    val count: Int = 0,
    val next: String = "",
    val previous: Any = Any(),
    val dataPokemons: List<Pokemon> = emptyList()
)
