package com.example.aplazo.domain.usecase

import com.example.aplazo.domain.BaseResultV2
import com.example.aplazo.domain.model.Catalog
import com.example.aplazo.domain.model.Pokemon
import com.example.aplazo.domain.model.Pokemons
import kotlinx.coroutines.flow.Flow

interface IGetCatalogs {

    suspend operator fun invoke(limit: String) : Flow<BaseResultV2<Pokemons,String>>
}