package com.example.aplazo.domain.usecase

import com.example.aplazo.data.network.response.DataDetailPokemon
import com.example.aplazo.domain.BaseResultV2
import kotlinx.coroutines.flow.Flow

interface IGetPokemonDetail {

    suspend operator fun invoke(mealId: String) : Flow<BaseResultV2<DataDetailPokemon?,String>>
}