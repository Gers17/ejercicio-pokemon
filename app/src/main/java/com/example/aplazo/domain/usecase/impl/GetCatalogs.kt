package com.example.aplazo.domain.usecase.impl

import com.example.aplazo.data.repository.PokemonRepository
import com.example.aplazo.domain.BaseResultV2
import com.example.aplazo.domain.model.Pokemon
import com.example.aplazo.domain.model.Pokemons
import com.example.aplazo.domain.usecase.IGetCatalogs
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class GetCatalogs(private val repository: PokemonRepository) : IGetCatalogs {

    override suspend fun invoke(limit: String): Flow<BaseResultV2<Pokemons, String>> = flow{
        emit(repository.getPokemons(limit))
    }
}