package com.example.aplazo.domain.usecase.impl

import com.example.aplazo.data.network.response.DataDetailPokemon
import com.example.aplazo.data.repository.PokemonRepository
import com.example.aplazo.domain.BaseResultV2
import com.example.aplazo.domain.usecase.IGetPokemonDetail
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class GetPokemonDetail(private val repository: PokemonRepository) : IGetPokemonDetail {
    override suspend fun invoke(mealId: String): Flow<BaseResultV2<DataDetailPokemon?, String>> = flow{
        emit(repository.getDetailPokemon(mealId))
    }


}