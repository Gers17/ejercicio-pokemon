package com.example.aplazo.ui.catalogs

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.aplazo.R
import com.example.aplazo.databinding.ActivityCatalogsBinding
import com.example.aplazo.utils.FragmentCommunication
import com.example.aplazo.utils.FragmentInflater
import sengiapps.loadsapp.extension.viewBinding

class CatalogsActivity : AppCompatActivity(), FragmentCommunication{

    private val binding by viewBinding(ActivityCatalogsBinding::inflate)
    private lateinit var fragmentInflater: FragmentInflater



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setup()
    }

    private fun setup(){
        fragmentInflater = FragmentInflater(supportFragmentManager)
        with(binding.toolbar.toolbar) {
            AppCompatResources.getDrawable(this@CatalogsActivity,  R.drawable.ic_arrow_back)
                .let { navigationIcon = it }
            setNavigationOnClickListener { onBackPressed() }
            title = "Buscar Pokemon"
        }

        updateFragmentContainer(CatalogsFragment.getInstance(), true)
    }

    override fun updateFragmentContainer(fragment: Fragment, isReplacement: Boolean) {
        fragmentInflater.updateFragmentContainer(
            fragment,
            binding.container.id,
            isReplacement
        )
    }
}