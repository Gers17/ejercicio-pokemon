package com.example.aplazo.ui.catalogs

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.aplazo.databinding.ItemCatalogBinding
import com.example.aplazo.databinding.LayoutFooterListBinding
import com.example.aplazo.domain.model.Catalog
import com.example.aplazo.domain.model.Pokemon
import com.example.aplazo.utils.LoaderViewHolder
import sengiapps.loadsapp.extension.loadUrl
import sengiapps.loadsapp.extension.singleClick
import sengiapps.loadsapp.extension.upperCaseFirstChar
import kotlin.properties.Delegates

class CatalogsAdapter( private val listener: ((item: String) -> Unit)

) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var isLoading: Boolean = false

    private var items: List<Pokemon?> by Delegates.observable(emptyList()) { _, old, new ->
        DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun getOldListSize(): Int = old.size

            override fun getNewListSize(): Int = new.size

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return old[oldItemPosition]?.name == new[newItemPosition]?.name
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return old[oldItemPosition] == new[newItemPosition]
            }
        }).dispatchUpdatesTo(this)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            1 -> {
                CatalogsViewHolder(
                    ItemCatalogBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
            else -> LoaderViewHolder(
                LayoutFooterListBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CatalogsAdapter.CatalogsViewHolder) {
            items[position]?.let { holder.bindItem(it, listener) }
        }
    }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int {
        return if (isLoading && position == itemCount - 1) 0
        else 1
    }

    fun updateItems(items: List<Pokemon>) {
        this.items = items
    }

    fun addMoreItems(items: List<Pokemon>) {
        this.items = this.items.filterNotNull() + items
        this.isLoading = false
    }

    fun addLoader() {
        this.isLoading = true
        this.items += null
    }


    private inner class CatalogsViewHolder(
        private val binding: ItemCatalogBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindItem(item: Pokemon, listener: ((item: String) -> Unit)) {
            val position = adapterPosition +1
            binding.ivCatalog.loadUrl("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${adapterPosition+1}.png")
            binding.tvNameCatalog.text = item.name.upperCaseFirstChar()
            //binding.tvCatalogDescription.text = item.description
            itemView.singleClick { listener.invoke(position.toString()) }
        }
    }
}