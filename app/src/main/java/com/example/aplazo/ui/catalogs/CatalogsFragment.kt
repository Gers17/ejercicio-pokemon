package com.example.aplazo.ui.catalogs

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.example.aplazo.databinding.FragmentCatalogsBinding
import com.example.aplazo.ui.detailpokemon.PokemonDetailActivity
import com.example.aplazo.utils.FragmentCommunication
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.android.ext.android.inject
import sengiapps.loadsapp.extension.hideKeyboard
import sengiapps.loadsapp.extension.singleClick
import sengiapps.loadsapp.extension.toast


class CatalogsFragment : Fragment() {

    private val viewModel: CatalogsViewModel by inject()

    private var activityListener: FragmentCommunication? = null
    private lateinit var binding: FragmentCatalogsBinding
    private lateinit var adapter: CatalogsAdapter
    private var limit = ""

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FragmentCommunication) activityListener = context
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentCatalogsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initObservers()
        setup()
    }

    private fun setup() {

        with(binding) {
            adapter = CatalogsAdapter {
                startActivity(PokemonDetailActivity.newIntent(requireContext(), it))
            }.also {
                rvCatalogs.adapter = it }

            srlCatalogs.setOnRefreshListener {
                srlCatalogs.isRefreshing = false
                hideKeyboard()
                if (limit.isBlank()){
                    requireContext().toast("No puedes dejar el campo vacio")
                }else{
                    fetchCatalogs(limit)
                }

            }

            btSearch.singleClick {
                limit = etLimit.text.toString()
                hideKeyboard()
                if (limit.isBlank()){
                    requireContext().toast("No puedes dejar el campo vacio")
                }else{
                    fetchCatalogs(limit)
                }
            }

        }
    }



    private fun fetchCatalogs(limit : String) {
        viewModel.getCatalogs(limit,"Cargando Pokemons")
    }


    private fun initObservers() {
        viewModel.uiState.flowWithLifecycle(
            viewLifecycleOwner.lifecycle,
            Lifecycle.State.STARTED
        ).onEach { state ->
            when(state){
                CatalogsViewState.EmptyCatalogs -> requireContext().toast("Sin elementos")
                is CatalogsViewState.Error -> requireContext().toast(state.statusMessage)
                is CatalogsViewState.Loading -> requireContext().toast("loading",Toast.LENGTH_LONG)
                is CatalogsViewState.Success -> {
                    binding.srlCatalogs.isVisible = true
                    adapter.updateItems(state.result)
                }

            }

        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    companion object {
        fun getInstance() = CatalogsFragment().apply {
        }
    }
}