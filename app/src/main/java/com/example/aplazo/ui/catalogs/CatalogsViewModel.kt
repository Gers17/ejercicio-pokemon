package com.example.aplazo.ui.catalogs

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.aplazo.domain.BaseResultV2
import com.example.aplazo.domain.usecase.IGetCatalogs
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class CatalogsViewModel(
    val getCatalogsUseCase: IGetCatalogs
) : ViewModel() {

    private val _uiState: MutableStateFlow<CatalogsViewState> =
        MutableStateFlow(CatalogsViewState.Start)
    val uiState: StateFlow<CatalogsViewState> = _uiState

    fun getCatalogs(limit: String, messageLoading: String) {
        viewModelScope.launch {
            getCatalogsUseCase(limit).onStart {
                _uiState.value = CatalogsViewState.Loading(true, messageLoading)
            }.onCompletion {
                _uiState.value = CatalogsViewState.Start
            }.collect { result ->
                when (result) {
                    is BaseResultV2.Error -> _uiState.value = CatalogsViewState.Error(result.error)
                    is BaseResultV2.Success -> {
                        if (result.data.dataPokemons.isNotEmpty()) {
                            _uiState.value =
                                CatalogsViewState.Success(result.data.dataPokemons)
                        } else {
                            _uiState.value =
                                CatalogsViewState.EmptyCatalogs
                        }
                    }
                }

            }
        }
    }


}