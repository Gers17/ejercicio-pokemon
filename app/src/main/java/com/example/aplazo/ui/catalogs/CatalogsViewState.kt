package com.example.aplazo.ui.catalogs

import com.example.aplazo.domain.model.Pokemon

sealed class CatalogsViewState{
    object Start : CatalogsViewState()
    data class Loading(val isLoading: Boolean, val messageLoading: String= "") : CatalogsViewState()
    data class Success(val result: List<Pokemon>) : CatalogsViewState()
    data class Error(val statusMessage: String) : CatalogsViewState()
    object EmptyCatalogs : CatalogsViewState()
}
