package com.example.aplazo.ui.detailpokemon

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import com.example.aplazo.R
import com.example.aplazo.databinding.ActivityOptionsMealBinding
import com.example.aplazo.utils.FragmentCommunication
import com.example.aplazo.utils.FragmentInflater
import sengiapps.loadsapp.extension.viewBinding

class PokemonDetailActivity : AppCompatActivity(), FragmentCommunication {

    private lateinit var fragmentInflater: FragmentInflater
    private val binding by viewBinding(ActivityOptionsMealBinding::inflate)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setup()
    }
    private fun setup() {
        fragmentInflater = FragmentInflater(supportFragmentManager)
        with(binding.toolbar.toolbar) {
            AppCompatResources.getDrawable(this@PokemonDetailActivity, R.drawable.ic_arrow_back)
                .let { navigationIcon = it }
            setSupportActionBar(this)
            val actionBar = supportActionBar
            actionBar?.title = "Detalle de Pokemon "
        }
        binding.toolbar.toolbar.setNavigationOnClickListener { onBackPressed() }
        val pokemonId = intent.extras?.getString("pokemonId")
        if (!pokemonId.isNullOrEmpty()) {
            updateFragmentContainer(PokemonDetailFragment.getInstance(pokemonId), true)
        }
    }

    override fun updateFragmentContainer(fragment: Fragment, isReplacement: Boolean) {
        fragmentInflater.updateFragmentContainer(
            fragment,
            binding.container.id,
            isReplacement
        )
    }


    companion object {
        fun newIntent(context: Context, optionMealId: String) =
            Intent(context, PokemonDetailActivity::class.java).apply {
                putExtra("pokemonId", optionMealId)
            }
    }
}