package com.example.aplazo.ui.detailpokemon

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.example.aplazo.data.network.response.DataDetailPokemon
import com.example.aplazo.databinding.FragmentMealDetailBinding
import com.example.aplazo.utils.FragmentCommunication
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.android.ext.android.inject
import sengiapps.loadsapp.extension.loadUrl
import sengiapps.loadsapp.extension.putArgs
import sengiapps.loadsapp.extension.toast
import sengiapps.loadsapp.extension.upperCaseFirstChar


class PokemonDetailFragment : Fragment() {

    val viewModel: PokemonDetailViewModel by inject()


    private lateinit var binding: FragmentMealDetailBinding
    private var activityListener: FragmentCommunication? = null
    private  lateinit var pokemonId : String




    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FragmentCommunication) activityListener = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pokemonId = requireArguments().getString("pokemonId").toString()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMealDetailBinding.inflate(inflater,container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initObservers()
        setup()
    }

    private fun setup() {
        viewModel.getPokemonDetail(pokemonId, "cargando detalles")
    }

    private fun updateView(pokemon: DataDetailPokemon){

        var types = ""
        with(binding){
            tvDetails.text = "Número en la pokedex: "+pokemon.id
            tvHeight.text = "Altura: "+pokemon.height +"ft"
            tvBaseExperience.text = "Experiencia: "+pokemon.base_experience +"lvl"
            tvWeight.text = "Peso: "+pokemon.weight +"lb"
            pokemon.types?.let {
                if (it.size >= 2){
                    types = it[0].type?.name.toString() + ", " + it[1].type?.name

                }else{
                    types = it[0].type?.name.toString()
                }
            }
            tvTypes.text = "Tipo: " + types
            tvNameDetail.text = pokemon.name?.upperCaseFirstChar()
            ivDetailPokemon.loadUrl("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemonId}.png")

        }
    }


    private fun initObservers() {
        viewModel.uiState.flowWithLifecycle(
            viewLifecycleOwner.lifecycle,
            Lifecycle.State.STARTED
        ).onEach {  state ->
            when (state){
                is PokemonDetailViewState.Error -> {
                    Snackbar.make(requireView(),"Error de servicio ${state.statusMessage}", Snackbar.LENGTH_LONG).show()
                }

                is PokemonDetailViewState.Loading -> requireContext().toast("loading", Toast.LENGTH_LONG)

                is PokemonDetailViewState.Success -> {
                    updateView(state.result)
                }

            }

        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }



    companion object {
        fun getInstance(mealId: String) = PokemonDetailFragment().putArgs {
            putString("pokemonId", mealId)
        }
    }
}