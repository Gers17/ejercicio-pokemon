package com.example.aplazo.ui.detailpokemon

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.aplazo.domain.BaseResultV2
import com.example.aplazo.domain.usecase.IGetPokemonDetail
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class PokemonDetailViewModel(
    private val getPokemonDetailUseCase: IGetPokemonDetail
) : ViewModel() {
    private val _uiState: MutableStateFlow<PokemonDetailViewState> =
        MutableStateFlow(PokemonDetailViewState.Start)
    val uiState: StateFlow<PokemonDetailViewState> = _uiState


   fun getPokemonDetail(mealId: String, messageLoading: String){
        viewModelScope.launch {
            getPokemonDetailUseCase(mealId).onStart {
                _uiState.value = PokemonDetailViewState.Loading(true,messageLoading)
            }.onCompletion {
                _uiState.value = PokemonDetailViewState.Start
            }.collect { result ->
                when(result){
                    is BaseResultV2.Error ->  _uiState.value = PokemonDetailViewState.Error(result.error)
                    is BaseResultV2.Success -> {
                            if (result.data != null){
                                _uiState.value =
                                    PokemonDetailViewState.Success(result.data)
                            }else{
                                _uiState.value =
                                    PokemonDetailViewState.EmptyPokemon
                            }
                    }
                }

            }
        }
    }
}