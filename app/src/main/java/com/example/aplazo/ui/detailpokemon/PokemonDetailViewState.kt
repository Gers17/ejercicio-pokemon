package com.example.aplazo.ui.detailpokemon

import com.example.aplazo.data.network.response.DataDetailPokemon

sealed class PokemonDetailViewState{
    object Start : PokemonDetailViewState()
    data class Loading(val isLoading: Boolean, val messageLoading: String= "") : PokemonDetailViewState()
    data class Success(val result: DataDetailPokemon) : PokemonDetailViewState()
    data class Error(val statusMessage: String) : PokemonDetailViewState()
    object EmptyPokemon : PokemonDetailViewState()
}
