package com.example.aplazo.utils

import android.util.Log
import com.example.aplazo.BuildConfig

class AppLog {

    companion object {
        @JvmStatic
        fun d(tag: String, message: String) {
            if (BuildConfig.DEBUG) {
                Log.d(tag, message)
            }
        }

        @JvmStatic
        fun e(tag: String, message: String, throwable: Throwable? = null) {
            if (BuildConfig.DEBUG) {
                Log.e(tag, message, throwable)
            }
        }
    }

}