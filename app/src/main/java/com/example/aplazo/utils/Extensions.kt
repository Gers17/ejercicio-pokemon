package sengiapps.loadsapp.extension

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.graphics.drawable.RippleDrawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import com.example.aplazo.R
import com.example.aplazo.utils.SingleClickListener
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.util.cio.*
import kotlinx.coroutines.io.copyAndClose
import kotlinx.coroutines.io.jvm.javaio.copyTo
import java.io.File
import java.io.OutputStream
import java.util.*

inline fun <T : ViewBinding> AppCompatActivity.viewBinding(crossinline bindingInflater: (LayoutInflater) -> T) =
    lazy {
        bindingInflater.invoke(layoutInflater)
    }

inline fun <FRAGMENT : Fragment> FRAGMENT.putArgs(argsBuilder: Bundle.() -> Unit): FRAGMENT =
    this.apply { arguments = Bundle().apply(argsBuilder) }

fun Fragment.isSafeFragment(): Boolean {
    return !this.isRemoving && this.activity != null && !this.isDetached && this.isAdded && this.view != null
}



fun Context.toast(message: String, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, length).show()
}

fun ImageView.loadUrl(url: String) {
    Glide.with(this)
        .load(url)
        .into(this)
}

fun ImageView.loadDrawable(drawable: Drawable) {
    Glide.with(this)
        .load(drawable)
        .into(this)
}

fun View.singleClick(view: (View) -> Unit) {
    setOnClickListener(SingleClickListener(view))
}

fun View.addRippleBackground(
    @ColorRes pressedColorId: Int = R.color.colorGrayLight2,
    @DrawableRes bgDrawableId: Int = 0,
    @ColorRes bgColorId: Int = 0
) {
    val drawable =
        if (bgDrawableId != 0) AppCompatResources.getDrawable(context, bgDrawableId)
        else this.background
    if (bgColorId != 0) {
        val color = ContextCompat.getColor(context, bgColorId)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            BlendModeColorFilter(color, BlendMode.SRC_IN).let { drawable?.colorFilter = it }
        } else {
            drawable?.setColorFilter(color, PorterDuff.Mode.SRC_IN)
        }
    }
    val pressedColor = ContextCompat.getColor(context, pressedColorId)
    this.background = getRippleDrawable(pressedColor, drawable)
}

private fun getRippleDrawable(pressedColor: Int, backgroundDrawable: Drawable?): RippleDrawable {
    return RippleDrawable(getPressedState(pressedColor), backgroundDrawable, null)
}

private fun getPressedState(pressedColor: Int): ColorStateList {
    return ColorStateList(arrayOf(intArrayOf()), intArrayOf(pressedColor))
}

fun File.writeBitmap(bitmap: Bitmap, format: Bitmap.CompressFormat, quality: Int) {
    outputStream().use { out ->
        bitmap.compress(format, quality, out)
        out.flush()
    }
}

suspend fun HttpClient.downloadFile(
    file: File,
    url: String,
    callback: suspend (boolean: Boolean) -> Unit
) {
    val call = call {
        url(url)
        method = HttpMethod.Get
    }
    if (!call.response.status.isSuccess()) {
        callback(false)
    }
    call.response.content.copyAndClose(file.writeChannel())
    callback(true)
}

suspend fun HttpClient.downloadFile(url: String, outputStream: OutputStream): Boolean {
    val call = call {
        url(url)
        method = HttpMethod.Get
    }
    if (!call.response.status.isSuccess()) {
        return false
    }
    call.response.content.copyTo(outputStream)
    return true
}

fun String.upperCaseFirstChar(locale: Locale = Locale.getDefault()) = replaceFirstChar {
    if (it.isLowerCase()) it.titlecase(locale) else it.toString()
}


fun AppCompatActivity.hideKeyboard() {
    val view = this.currentFocus
    if (view != null) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
    window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
}

fun Fragment.hideKeyboard() {
    val activity = this.activity
    if (activity is AppCompatActivity) {
        activity.hideKeyboard()
    }
}