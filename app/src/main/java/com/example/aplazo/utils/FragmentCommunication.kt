package com.example.aplazo.utils

import androidx.fragment.app.Fragment

interface FragmentCommunication {

    fun updateFragmentContainer(fragment: Fragment, isReplacement: Boolean = false)


}