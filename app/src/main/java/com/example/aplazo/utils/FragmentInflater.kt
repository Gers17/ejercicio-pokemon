package com.example.aplazo.utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

class FragmentInflater(
    private val fragmentManager: FragmentManager
) {

    fun updateFragmentContainer(fragment: Fragment, containerId: Int, isReplacement: Boolean = true) {
        with(fragmentManager) {
            val ft = beginTransaction()
            if (isReplacement) {
                if (backStackEntryCount > 0) {
                    popBackStack(
                        getBackStackEntryAt(0).id,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE
                    )
                }
                ft.replace(containerId, fragment)
            } else {
                findFragmentById(containerId).let {
                    if (it != null) {
                        ft.hide(it)
                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    }
                    ft.addToBackStack(null)
                    ft.add(containerId, fragment)
                }
            }
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            ft.commit()
        }
    }

}