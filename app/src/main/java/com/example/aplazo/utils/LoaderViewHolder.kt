package com.example.aplazo.utils

import androidx.recyclerview.widget.RecyclerView
import com.example.aplazo.databinding.LayoutFooterListBinding

class LoaderViewHolder(binding: LayoutFooterListBinding) : RecyclerView.ViewHolder(binding.root)