package com.example.aplazo.utils

import android.view.View

class SingleClickListener(private val click: (view: View) -> Unit) : View.OnClickListener {

    private var lastClick: Long = 0

    override fun onClick(v: View) {
        val currentTimeClick = System.currentTimeMillis()
        val elapsedTime = currentTimeClick - lastClick
        lastClick = currentTimeClick
        if (elapsedTime > DOUBLE_CLICK_TIMEOUT) {
            click(v)
        }
    }

    companion object {
        private const val DOUBLE_CLICK_TIMEOUT = 600L
    }

}